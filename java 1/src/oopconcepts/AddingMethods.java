package oopconcepts;

public class AddingMethods {

	public static void main(String[] args) {
		String Grade;
		String studentName;
		
		studentName="John";
		Grade = findGrades(100);
		displayGrades(studentName,Grade);
		
		studentName="Sam";
		Grade = findGrades(10.9);
		displayGrades(studentName,Grade);
	}
	
	public static String findGrades(double Score) {
	
		String grade;
		
		if (Score > 90 && Score <=100){
			grade="A";
		}else if (Score > 80 && Score <=90){
			grade="B";
		}else if (Score >70 && Score <=80){
			grade="C";
		}else{
			grade="D";}	
		
		return grade;
		
}
	
	public static void displayGrades(String StudentName,String Grade) {
		System.out.println("Grade of "+ StudentName +" is: " + Grade);
	}
}