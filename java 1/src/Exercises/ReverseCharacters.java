package Exercises;

import java.util.Scanner;

public class ReverseCharacters {

	public static void main(String[] args) {
		Scanner Scan= new Scanner(System.in);
		System.out.println("Please enter a string");
		String input=Scan.nextLine();
		
		while (input.isEmpty() || input==null) {
			System.out.println("Please enter a valid string");
			input=Scan.nextLine();
		}
		Scan.close();
		
		ReverseCharacters output= new ReverseCharacters();
		String RevChar = output.ReverseCharacter(input);
		System.out.println(RevChar);
		
	}
	
	private String ReverseCharacter(String Input) {
		String Reverse="";
		
		for (int i=Input.length()-1; i>=0;  i--) {
			Reverse=Reverse + Input.charAt(i);			
		}
		return Reverse;
	}
	}
	


